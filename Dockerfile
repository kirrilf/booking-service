FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=*.jar
COPY target/bookingService-2.5.4.jar /bookingService.jar
COPY springboot.p12 /

ENTRYPOINT ["java", "-jar", "/bookingService.jar"]