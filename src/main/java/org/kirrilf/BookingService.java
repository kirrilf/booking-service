package org.kirrilf;


import com.google.gson.Gson;
import org.kirrilf.exception.ApiRequestException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@SpringBootApplication
public class BookingService {
  public static void main(String[] args) {
        SpringApplication.run(BookingService.class, args);
    }

}
