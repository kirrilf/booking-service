package org.kirrilf.api;

import java.util.List;

public interface FlightApi {

    void flightValidation(List<String> flightCodes);

    void bookSeats(String flightCode, Integer count);

    void removeBookSeats(String flightCode, Integer count);

}
