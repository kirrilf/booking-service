package org.kirrilf.api.iml;

import com.google.gson.Gson;
import org.kirrilf.api.FlightApi;
import org.kirrilf.exception.ApiException;
import org.kirrilf.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Service
public class FlightApiImpl implements FlightApi { //TODO rewrite all impl

    private final String FLIGHT_VALIDATION_URL = "/flight/validation";
    private final String FLIGHT_BOOK_SEATS_URL = "/flight/book-seats";
    private final String FLIGHT_REMOVE_BOOK_SEATS_URL = "/flight/remove-book-seats/";

    private final String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhYWFhIiwicm9sZXMiOltdLCJpYXQiOjE2Mzc3NjU3MjUsImV4cCI6MjYzNzc2NjMyNX0.bYnZNH2ecWA7IEXPBo3XLA_GHnF9yFEsKLawURfXY-k";

    @Value("${flightService.url}")
    private String flightServiceURL;

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    @Override
    public void flightValidation(List<String> flightCodes) {
        try {
            HttpRequest.BodyPublisher bodyPublisher = HttpRequest.BodyPublishers.ofString(new Gson().toJson(flightCodes));
            HttpRequest request = HttpRequest.newBuilder()
                    .POST(bodyPublisher)
                    .uri(URI.create(flightServiceURL + FLIGHT_VALIDATION_URL))
                    .setHeader("User-Agent", "Java 11 HttpClient Bot")
                    .setHeader("Authorization", token)// add request header
                    .header("Content-Type", "application/json")
                    .build();

                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                ifBadResponse(response);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new ApiRequestException("Internal ERROR", e);
        }
    }

    @Override
    public void bookSeats(String flightCode, Integer count) {
        sendGetRequest(flightCode, count, FLIGHT_BOOK_SEATS_URL);

    }

    @Override
    public void removeBookSeats(String flightCode, Integer count) {
        sendGetRequest(flightCode, count, FLIGHT_REMOVE_BOOK_SEATS_URL);
    }

    private void ifBadResponse(HttpResponse<String> response){
        if (response.statusCode() != 200) {
            ApiException apiRequestException = new Gson().fromJson(response.body(), ApiException.class);
            throw new ApiRequestException(apiRequestException.getMessage());
        }
    }

    private void sendGetRequest(String flightCode, Integer count, String flight_remove_book_seats_url) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(flightServiceURL + flight_remove_book_seats_url + "/" + flightCode + "/" + count))
                    .setHeader("Authorization", token)
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ifBadResponse(response);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
