package org.kirrilf.contorller;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.kirrilf.dto.request.BookingRequest;
import org.kirrilf.dto.response.BookingResponse;
import org.kirrilf.service.BookingService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/booking")
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;


    @PostMapping
    public BookingResponse createBooking(@RequestBody BookingRequest bookingRequest){
        return bookingService.createBooking(bookingRequest);
    }

    @GetMapping("/pay/{paymentCode}")
    public BookingResponse paidBooking(@PathVariable String paymentCode){
        return bookingService.payBooking(paymentCode);
    }


    @GetMapping("/{paymentCode}")
    public BookingResponse getBooking(@PathVariable String paymentCode){
        return bookingService.getBooking(paymentCode);
    }





}
