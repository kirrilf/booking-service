package org.kirrilf.dto.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingRequest {

    private String phoneNumber;

    @JsonProperty("passengers")
    private List<PassengerRequest> passengers;

    @JsonProperty("flights")
    private List<String> flightCodes;

}
