package org.kirrilf.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class PassengerRequest {

    private  String firstName;

    private String lastName;

    private String passportCode;

}
