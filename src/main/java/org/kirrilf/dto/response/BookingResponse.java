package org.kirrilf.dto.response;


import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
public class BookingResponse {

    private String bookingCode;

    private String paymentCode;

    private String bookingStatus;

    private Integer amountPassengers;

    private List<PassengerResponse> passengers;

    private String phoneNumber;


}
