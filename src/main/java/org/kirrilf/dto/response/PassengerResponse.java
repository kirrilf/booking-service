package org.kirrilf.dto.response;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PassengerResponse {

    private String firstName;

    private String lastName;

}
