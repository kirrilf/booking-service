package org.kirrilf.dto.response;


import lombok.Builder;
import lombok.Data;
import org.kirrilf.model.Passenger;

import java.util.List;

@Data
@Builder
public class TicketResponse {

    private String ticketCode;

    private Passenger passenger;

    private List<String> flights;

}
