package org.kirrilf.exception;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
public enum ErrorCode {

    WRONG_FIND_PARAMETERS(3131, "Invalid search parameters");



    private final Integer code;
    private final String message;



}
