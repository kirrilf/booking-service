package org.kirrilf.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "booking")
@Getter
@Setter
public class Booking extends BaseEntity{

    @Column(name = "booking_code", unique = true)
    private String bookingCode;

    @Column(name = "payment_code", unique = true)
    private String paymentCode;

    @Column(name = "phone_number", unique = true)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "booking_status")
    private BookingStatus bookingStatus;

    @Column(name = "amount_passengers")
    private Integer amountPassengers;

   @ManyToMany(cascade = CascadeType.ALL)
    private List<Passenger> passengers;

    @ElementCollection
    @CollectionTable(name = "flight_codes")
    @Column(name = "flight_code")
    private List<String> flightCodes;



}
