package org.kirrilf.model;

public enum BookingStatus {
    PAID, NOT_PAID
}
