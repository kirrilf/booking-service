package org.kirrilf.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "passenger")
@Getter
@Setter
public class Passenger extends BaseEntity{

    private String firstName;

    private String lastName;

    private String passportCode;

}
