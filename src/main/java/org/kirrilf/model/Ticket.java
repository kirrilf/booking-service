package org.kirrilf.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
@Getter
@Setter
public class Ticket extends BaseEntity{

    @Column(name = "ticketCode")
    private String ticketCode;

    @OneToOne
    private Passenger passenger;

    @ManyToOne
    private Booking booking;


}
