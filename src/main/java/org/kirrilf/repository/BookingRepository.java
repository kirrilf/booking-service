package org.kirrilf.repository;

import org.kirrilf.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@EnableJpaRepositories
public interface BookingRepository extends JpaRepository<Booking, Long> {

    Optional<Booking> findBookingByBookingCode(String bookingCode);

    Optional<Booking> findBookingByPaymentCode(String paymentCode);

}
