package org.kirrilf.repository;

import org.kirrilf.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@EnableJpaRepositories
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Optional<Ticket> findTicketByTicketCode(String ticketCode);

}
