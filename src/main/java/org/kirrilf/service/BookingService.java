package org.kirrilf.service;


import org.kirrilf.dto.request.BookingRequest;
import org.kirrilf.dto.response.BookingResponse;

import java.io.IOException;

public interface BookingService {

    BookingResponse createBooking(BookingRequest bookingRequest);

    BookingResponse payBooking(String paymentCode);

    BookingResponse getBooking(String bookingCode);
}
