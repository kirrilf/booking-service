package org.kirrilf.service;

import org.kirrilf.dto.response.TicketResponse;
import org.kirrilf.model.Booking;

import java.util.List;

public interface TicketService {

    List<TicketResponse> createTickets(Booking booking);

}
