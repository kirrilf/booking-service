package org.kirrilf.service.impl;

import lombok.RequiredArgsConstructor;
import org.kirrilf.api.FlightApi;
import org.kirrilf.dto.request.BookingRequest;
import org.kirrilf.dto.request.PassengerRequest;
import org.kirrilf.dto.response.BookingResponse;
import org.kirrilf.dto.response.PassengerResponse;
import org.kirrilf.exception.ApiRequestException;
import org.kirrilf.exception.ErrorCode;
import org.kirrilf.model.Booking;
import org.kirrilf.model.BookingStatus;
import org.kirrilf.model.Passenger;
import org.kirrilf.repository.BookingRepository;
import org.kirrilf.service.BookingService;
import org.kirrilf.service.TicketService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final TicketService ticketService;
    private final FlightApi flightApi;


    @Override
    @Transactional
    public BookingResponse createBooking(BookingRequest bookingRequest) {
        bookingFlightsValidation(bookingRequest.getFlightCodes());
        bookSeats(bookingRequest.getFlightCodes(), bookingRequest.getPassengers().size());
        return mapToResponse(bookingRepository.save(buildBooking(bookingRequest)));
    }

    @Override
    @Transactional
    public BookingResponse payBooking(String paymentCode) {
        Booking booking = bookingRepository.findBookingByPaymentCode(paymentCode).orElseThrow(() -> new ApiRequestException(ErrorCode.WRONG_FIND_PARAMETERS.toString()));
        booking.setBookingStatus(BookingStatus.PAID);
        ticketService.createTickets(booking);
        return mapToResponse(booking);
    }

    @Override
    public BookingResponse getBooking(String bookingCode) {
        return mapToResponse(bookingRepository.findBookingByBookingCode(bookingCode).orElseThrow(()->new ApiRequestException(ErrorCode.WRONG_FIND_PARAMETERS.toString())));
    }

    private BookingResponse mapToResponse(Booking booking) {
        return BookingResponse.builder()
                .bookingStatus(booking.getBookingStatus().name())
                .bookingCode(booking.getBookingCode())
                .paymentCode(booking.getPaymentCode())
                .phoneNumber(booking.getPhoneNumber())
                .passengers(booking.getPassengers().stream().map(this::mapPassengerToResponse).collect(Collectors.toList()))
                .amountPassengers(booking.getPassengers().size())
                .build();
    }

    private void bookingFlightsValidation(List<String> flights) {
        flightApi.flightValidation(flights);
    }

    private void bookSeats(List<String> flightCodes, int amount) {
        flightCodes.forEach(it -> flightApi.bookSeats(it, amount));
    }

    private PassengerResponse mapPassengerToResponse(Passenger passenger) {
        return PassengerResponse.builder()
                .firstName(passenger.getFirstName())
                .lastName(passenger.getLastName())
                .build();
    }

    private Booking buildBooking(BookingRequest bookingRequest) {
        Booking booking = new Booking();
        booking.setBookingCode(UUID.randomUUID().toString().substring(6));
        booking.setBookingStatus(BookingStatus.NOT_PAID);
        booking.setAmountPassengers(bookingRequest.getPassengers().size());
        booking.setPaymentCode(UUID.randomUUID().toString().substring(8));
        booking.setPhoneNumber(bookingRequest.getPhoneNumber());
        booking.setPassengers(bookingRequest.getPassengers().stream().map(this::buildPassenger).collect(Collectors.toList()));
        booking.setFlightCodes(bookingRequest.getFlightCodes());
        return booking;
    }


    private Passenger buildPassenger(PassengerRequest passengerRequest) {
        Passenger passenger = new Passenger();
        passenger.setPassportCode(passengerRequest.getPassportCode());
        passenger.setFirstName(passengerRequest.getFirstName());
        passenger.setLastName(passengerRequest.getLastName());
        return passenger;
    }
}
