package org.kirrilf.service.impl;

import lombok.RequiredArgsConstructor;
import org.kirrilf.dto.response.TicketResponse;
import org.kirrilf.model.Booking;
import org.kirrilf.model.Ticket;
import org.kirrilf.repository.TicketRepository;
import org.kirrilf.service.TicketService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    @Override
    public List<TicketResponse> createTickets(Booking booking) {
            return ticketRepository.saveAll(
            IntStream.range(0, booking.getAmountPassengers())
                    .mapToObj(i -> buildTicket(booking, i))
                    .collect(Collectors.toList()))
                    .stream()
                    .map(this::mapToResponse)
                    .collect(Collectors.toList());

    }


    private Ticket buildTicket(Booking booking, int passengerNum){
        Ticket ticket = new Ticket();
        ticket.setTicketCode(booking.getBookingCode() + UUID.randomUUID().toString().substring(6));
        ticket.setBooking(booking);
        ticket.setPassenger(booking.getPassengers().get(passengerNum));
        return ticket;
    }

    private TicketResponse mapToResponse(Ticket ticket){
        return TicketResponse.builder()
                .ticketCode(ticket.getTicketCode())
                .passenger(ticket.getPassenger())
                .flights(ticket.getBooking().getFlightCodes())
                .build();
    }

}
